// mpirun -n 2 ./a.out

#include <stdio.h>
#include <mpi.h>

int main(
    void
){
  int nprocs = 0;
  int myrank = 0;
  MPI_Init(NULL, NULL);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  printf("hello world from rank %d / %d\n", myrank, nprocs);
  MPI_Finalize();
  return 0;
}

