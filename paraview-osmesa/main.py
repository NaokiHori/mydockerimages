#!/usr/bin/env pvpython

# ref: https://docs.paraview.org/en/latest/Tutorials/ClassroomTutorials/pythonAndBatchPvpythonAndPvbatch.html

from paraview.simple import *

size = [800, 800]

# Lets create a sphere
sphere = Sphere()
Show()
Render()

# get active view
renderView1 = GetActiveViewOrCreate("RenderView")
renderView1.ViewSize = size

# get display properties
sphere1Display = GetDisplayProperties(sphere, view=renderView1)

# change solid color
sphere1Display.AmbientColor = [0.0, 1.0, 0.0]
sphere1Display.DiffuseColor = [0.0, 1.0, 0.0]

# save screenshot
SaveScreenshot("sphere.png", renderView1, ImageResolution=size)
