reset

set terminal epslatex standalone color size 5.0,3.5 font ',17.28' header '\usepackage{xcolor}'
set output 'result.tex'

set xlabel '$\textcolor[HTML]{444444}{x}$'
set ylabel '$\textcolor[HTML]{888888}{y}$'

set xrange [0.:2.*pi]
set yrange [-1.1:1.1]

set xtics ('$0$' 0., '$\pi$' pi, '$2 \pi$' 2.*pi)
set ytics 0.5

set style line 1 lc rgb '#FF8888' lw 5 dt 1
set style line 2 lc rgb '#8888FF' lw 5 dt 2
set style line 3 lc rgb '#33AA00' lw 5 dt 3

set key top right spacing 1.2

plot \
  sin(x) t '$\textcolor[HTML]{FF2222}{\sin \left( x \right)}$' ls 1 w l, \
  cos(x) t '$\textcolor[HTML]{2222FF}{\cos \left( x \right)}$' ls 2 w l, \
  tan(x) t '$\textcolor[HTML]{33AA00}{\tan \left( x \right)}$' ls 3 w l

