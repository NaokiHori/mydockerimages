# Locally-hosted `MathJax`

Hosting `MathJax` locally

## Build Docker Image

```bash
docker build -t mathjax-local:latest .
```

## Run Container

```bash
docker run --rm -it -p 8080:80 mathjax-local:latest
```

## Usage

See `index.html`.

