#if !defined(MYFUNC_H)
#define MYFUNC_H

extern int add(
    const int a,
    const int b
);

#endif // MYFUNC_H
