#if defined(DO_UNITY_TEST)

#include <unity.h>
#include "myfunc.h"

void test1(
    void
){
  TEST_ASSERT_EQUAL_INT(5, add( 2, 3));
  TEST_ASSERT_EQUAL_INT(6, add( 3, 3));
  TEST_ASSERT_EQUAL_INT(0, add(-3, 3));
}

void setUp(
    void
){
}

void tearDown(
    void
){
}

int main(
    void
){
  UNITY_BEGIN();
  RUN_TEST(test1);
  return UNITY_END();
}

#endif // DO_UNITY_TEST
