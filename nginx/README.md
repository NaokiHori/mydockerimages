# Minimal `Nginx` Container

A minimal set of configuration files to use an `Nginx` http server.

## Build Docker Image

```bash
docker build -t minimal-nginx:latest .
```

## Run Container

```bash
docker run --rm -it -p 8080:80 minimal-nginx:latest
```

or

```bash
docker run --name minimal-nginx-container --rm -d -p 8080:80 minimal-nginx:latest
```

## Fetch Assets

```bash
curl http://localhost:8080/index.html
```

## Stop Container

```bash
Ctrl + C
```

or

```bash
docker stop minimal-nginx-container
```

for the former and latter run options, respectively.

