// https://gitlab.com/cunity/cunit#quick-start

#include <CUnit/CUnitCI.h>

// check very difficult math
static void test(void){
  CU_ASSERT_FATAL(2 == 1 + 1);
}

CUNIT_CI_RUN("my-suite", CUNIT_CI_TEST(test));

