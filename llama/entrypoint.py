import os

model = "/project/phi-2.Q4_K_M.llamafile"

def main():
    with open("instruct.txt", "r") as f:
        instruct = f.readlines()
    instruct = " ".join(instruct)
    prompt = ""
    prompt += "docker run"
    prompt += " --rm -it --workdir /project"
    prompt += " llama:latest"
    prompt += " bash {}".format(model)
    prompt += " --log-disable"
    prompt += " -n 256"
    prompt += " -p \"Input: repeat the following: {}\"".format(instruct)
    os.system(prompt)

main()
